package garage;

public class Voiture
{
    // variables d'instance
    private int nbportes;
    private int km;
    /* Deuxieme conducteur */
    private Proprietaire conducteur;

    /**
     * Constructeur d'objets de classe Voiture
     */
    public Voiture(int nbportes, int km)
    {
        // initialisation des variables d'instance
        this.nbportes = nbportes;
        this.km = km;
        this.conducteur = null;

    }

    //Retourner le nombre de portes de la voiture
    public int getNbportes(){
        return this.nbportes;
    }

    //Retourner le conducteur de la voiture
    public Proprietaire getConducteur() {
        return conducteur;
    }

    //Modifier le conducteur de la voiture
    public void setConducteur(Proprietaire conducteur) {
        this.conducteur = conducteur;
    }

    //Modifier le nombre de portes de la voiture
    private void setNbportes(int nbportes){
        this.nbportes = nbportes;
    }

    //Retourner le kilomètrage de la voiture
    public int getKm(){
        return this.km;
    }

    //Modifier le kilomètrage de la voiture
    public void setKm(int km){
        this.km = km;
    }

    //Changer le kilomètrage de la voiture après avoir rouler
    public int Rouler(int km){
        this.km += km;
        return km;
    }
}