package garage;

public class Proprietaire
{
    // Les attributs de la classe propritaire nom, voiture,...etc
    private String nom;
    private Voiture voiture;
    private int prix;
    private int duree;

    /**
     * Constructeur d'objets de classe Proprietaire
     */
    public Proprietaire(String nom, Voiture voiture, int prix, int duree)
    {
        // initialisation des attributs de la classe propriètaire
        this.nom =  nom;
        this.voiture = voiture;
        this.prix = prix;
        this.duree = duree;
    }

    public void setVoiture(Voiture voiture){
        this.voiture = voiture;
    }

    public Voiture getVoiture(){
        return voiture;
    }

    public void setPrixAchat(int prix){
        this.prix = prix;
    }

    public int getPrixAchat(){
        return prix;
    }
    public void setNom(String nom){
        this.nom = nom;
    }

    public String getNom(){
        return this.nom;
    }
    public void setDuree(int duree){
        this.duree = duree;
    }


    public int getDuree(){
        return this.duree;
    }
    public int prixVenteVoiture(int km){
        int nbkm = voiture.Rouler(km);
        int prixdevente = (int)((this.prix)/(0.1*(nbkm+this.duree)));
        return prixdevente;
    }

}
